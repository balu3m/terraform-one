provider "google" {
  credentials = "${file("green-orb-273714-d035f2cfb5f0.json")}"
  project     = "green-orb-273714"
  region      = "us-west1"
  zone        = "us-west1-a"
}
resource "random_id" "instance_id" {
 byte_length = 8
}

resource "google_compute_disk" "disk-test" {
  name  = "test-disk"
  size  = "12"
  type  = "pd-ssd"
  zone  = "us-west1-a"
  image = "debian-cloud/debian-9"
  labels = {
    environment = "dev"
  }
  physical_block_size_bytes = 4096
}

resource "google_compute_instance" "vm-test" {
 name         = "red-salud-test"
 machine_type = "f1-micro"
 zone         = "us-west1-a"
 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }

 network_interface {
   network = "default"
   access_config {
    }
 }
}
